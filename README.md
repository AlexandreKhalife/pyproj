# pyProj

<!-- start_overview -->
*A standard architecture of python package using API and CLI endpoints*

## Repository structure

```bash
.
├── .gitignore
├── .gitlab-ci.yml
├── Makefile
├── README.md
├── doc
│   └── ...
├── install-poetry.py
├── poetry.lock
├── pyproject.toml
├── setup.cfg
└── src
    ├── lib_1
    │   ├── __init__.py
    │   ├── api.py
    │   ├── cli.py
    │   ├── complex_pkg
    │   │   ├── __init__.py
    │   │   └── return_constant.py
    │   ├── multiplicator.py
    │   └── tests
    │       ├── e2e
    │       │   ├── test_api.py
    │       │   └── test_cli.py
    │       └── unit
    │           └── test_multiplicator.py
    └── lib_2
        ├── __init__.py
        └── api.py
```
<!-- end_overview -->

# Install

<!-- start_install -->
## Install Poetry

From your **bash terminal**, go to pyproj directory:
```bash
cd pyproj
```

From pyproj in your **bash terminal**:
```bash
chmod +x install-poetry.py
python3 install-poetry.py --version 1.4.2
```

## Install pyProj

From pyproj in your **bash terminal**:
```bash
poetry install
```
## Enable bash-autocompletion

From anywhere in your **bash terminal**:
```bash
echo 'eval "$(register-python-argcomplete pyproj)"' >> ~/.bashrc
source ~/.bashrc
```
<!-- end_install -->

# Usage

<!-- start_usage -->
## Use CLI EndPoints

From anywhere in your **bash terminal**:

```bash
cd /tmp

# Display CLI doc
pyproj -h

# Display sub-program doc
pyproj multiply -h

# Simple CLI module without parameter
pyproj print

# e.g. Complex CLI with argparse parameters
pyproj multiply -f 10 -n hello_world
```

### Use API EndPoints

From anywhere in your **python terminal**:

```python
# Import the lib
import lib_1 as l1

# Get help for lib funtion
help(l1.multiply_name_length)

# Run the lib function
result_1 = l1.multiply_name_length(name='hugo', factor=42)
result_2 = l1.multiply_number_with_random(12)
result_3 = l1.return_str_const("hello")
# ...
```
<!-- end_usage -->

# Run tests

<!-- start_test -->
## Run all tests

From pyproj in your **bash terminal**:

```bash
make tests
```

## OR run tests one by one

From pyproj in your **bash terminal**:

```bash
make qa_check_code
make unittest
make e2etest
```
<!-- end_test -->

# Document your project
<!-- start_doc -->

```{warning}
Current implementation assume you are working on WSL2.
If you are working on linux or mac, replace in Makefile `wslview` with `firefox`.
```

## Write your project documentation in `doc/`

Documentation use Sphinx & Furo Theme for web rendering:
1. Create `.md` or `.rst` files
2. Add your files to `index.md`

## Display the documentation

From pyproj in your **bash terminal**:
```bash
# dev mode: auto reload
make doc-autobuild

# stable generation
make doc-build
make doc-display
```
<!-- end_doc -->

**Documentation website output**

![doc_website](/doc/_static/documentation.png)
