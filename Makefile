.PHONY: tests

# Clean
clean:
	@find . -name '__pycache__' | xargs rm -fr {} \;
	@find . -name '.coverage' | xargs rm -fr {} \;
	@find . -name 'build' | xargs rm -fr {} \;
	@find . -name 'dist' | xargs rm -fr {} \;
	@find . -name '.pytest_cache' | xargs rm -fr {} \;
	@find . -type d -name '*.egg-info' | xargs rm -fr {} \;
	@find . -type d -name '*.eggs' | xargs rm -fr {} \;
	@make doc-clean

# Install
install:
	@poetry install

# Tests
qa_check_code:
	@find ./src -name "*.py" | xargs flake8 --exclude *.eggs*
unittest:
	@pytest -v --cov=src --cov-report term-missing src/**/tests/unit
e2etest:
	@pytest -v --cov=src --cov-report term-missing src/**/tests/e2e
tests:
	@make qa_check_code
	@make unittest
	@make e2etest

# Doc
doc-clean:
	@rm -fr doc/_build
doc-build:
	@make doc-clean && sphinx-build doc/ doc/_build
doc-display:
	@(wslview doc/_build/index.html) &
doc-autobuild:
	@make doc-clean
	@(wslview http://127.0.0.1:8000) &
	@sphinx-autobuild doc/ doc/_build
