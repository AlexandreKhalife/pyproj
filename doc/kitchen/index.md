# Markup Reference

Furo supports everything you can do with standard Sphinx, as well as few recommended third-party Sphinx extensions.

This section serves as a showcase for how things look in Furo, and provides a syntax reference for both [reStructuredText] and [Markdown (MyST)][myst-markdown].

See [furo customisation](https://pradyunsg.me/furo/customisation/>) to adapt the theme to your
needs


```{toctree}
:titlesonly:
:glob:

*
```

[myst-markdown]: https://myst-parser.readthedocs.io/en/latest/
[restructuredtext]: https://docutils.sourceforge.io/docs/user/rst/quickref.html
