from pathlib import Path
from pkg_resources import resource_filename
from lib_2.api import *

LIB_PATH = Path(resource_filename(__name__, "/"))

# module level doc-string
__docformat__ = "restructuredtext"
__doc__ = """
pyproj - An example of python package structure
===============================================

using:
- poetry
- flake8
- mypy
- gitlab-ci
"""
