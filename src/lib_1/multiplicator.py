def multiply_name_length(
    name: str = "alex",
    factor: float = 3
) -> float:
    """
    Multiply the number of letters in your name with a chosen factor
    """
    result = len(name) * factor
    print(f"{name} * {factor} = {result}")
    return result


def _rand(
) -> float:
    import random
    return random.random()


def multiply_number_with_random(
    number: float
) -> float:
    salt = _rand()
    result = number * salt
    print(f"{number} * {salt} = {result}")
    return result
