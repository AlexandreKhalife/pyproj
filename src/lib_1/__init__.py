from pathlib import Path
from lib_1.api import *

# module level doc-string
__docformat__ = "restructuredtext"
__doc__ = """
pyproj - An example of python package structure
===============================================

using:
- poetry
- flake8
- mypy
- gitlab-ci
"""
