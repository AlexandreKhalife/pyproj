import pytest
import lib_1 as l1


class TestAPIEndPoints():

    @pytest.fixture(scope="session", autouse=True)
    def setting_and_cleaner(self):
        # Do stuff before all tests
        print("init API test")

        yield

        # Do stuff after all tests
        print("clean API test")

    def test_return_float_const(self):
        output = l1.return_float_const()
        assert output == 42.42
        output = l1.return_float_const(42)
        assert output == 42
