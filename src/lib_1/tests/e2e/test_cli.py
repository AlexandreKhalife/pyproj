import os
import pytest


class TestCLIEndPoints():

    @pytest.fixture(scope="session", autouse=True)
    def setting_and_cleaner(self):
        # Do stuff before all tests
        print("init CLI test")

        yield

        # Do stuff after all tests
        print("clean CLI test")

    def test_pyproj_help(self):
        exit_code = os.system("pyproj -h")
        assert exit_code in [0, 256]

    def test_pyproj_print(self):
        exit_code = os.system("pyproj print")
        assert exit_code in [0, 256]

    def test_pyproj_multiply(self):
        exit_code = os.system("pyproj multiply -f 10 -n hello_world")
        assert exit_code in [0, 256]
