def return_float_const(
    constant: float = 42.42
) -> float:
    print(constant)
    return constant


def return_str_const(
    constant: str = "forty-two"
) -> str:
    print(constant)
    return constant
