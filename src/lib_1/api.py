from lib_1.complex_pkg.return_constant import (
    return_float_const,
    return_str_const
)

from lib_1.multiplicator import (
    multiply_name_length,
    multiply_number_with_random
)
