#!/usr/bin/env python
# PYTHON_ARGCOMPLETE_OK
import sys
import argparse
import argcomplete

from lib_1.multiplicator import multiply_name_length
from lib_1.complex_pkg.return_constant import return_str_const


def parse_arguments():
    """
    Main program
    ============
    """
    parser_main = argparse.ArgumentParser(
        prog="pyproj",
        description="An example of python package structure"
    )
    subparsers = parser_main.add_subparsers()
    subparsers.required = True

    """
    Sub-program: 'print'
    ====================
    """
    subparsers.add_parser(
        "print",
        description="Nothing important can be done with it",
        help="Print a constant"
    )

    """
    Sub-program: 'multiply'
    =======================
    """
    parser_multiply = subparsers.add_parser(
        "multiply",
        description="Nothing important can be done with it",
        help="Can multiply the length of a string with a value"
    )
    parser_multiply.add_argument(
        "-n", "--name",
        nargs='?',
        default="alex",
        type=str,
        help=("Here the string")
    )
    parser_multiply.add_argument(
        "-f", "--factor",
        nargs='?',
        default=3,
        type=float,
        help="Here the factor"
    )

    """
    Parse args
    ==========
    """
    argcomplete.autocomplete(parser_main)
    try:
        args = parser_main.parse_args()
        module = sys.argv[1]
    except TypeError:
        parser_main.error("invalide param choice")
        return None
    return module, args


def main():
    module, args = parse_arguments()
    if module == "print":
        return_str_const()
    if module == "multiply":
        name = args.name
        factor = args.factor
        multiply_name_length(name=name, factor=factor)
